#pragma once

struct GameState;

void init_ball(GameState* state);
void init_paddle(GameState* state);
void init_state(GameState* state);
