#pragma once

#include "types.h"

typedef struct GameState GameState;

typedef struct Rect {
  game_coord_t l,t,r,b;
} Rect;

typedef struct GameChanges {
  Rect erase_rects[NUM_OBJ];
  game_id_t redraw_objs[NUM_OBJ];
  uint8_t num_erases;
  uint8_t num_redraws;
} GameChanges;


void draw_ball(game_coord_t x, game_coord_t y);
void draw_paddle(game_coord_t x, game_coord_t y);
void draw_block(game_coord_t x, game_coord_t y);
void init_graphics(GameState* state);
void draw_changes(GameChanges* changes);
