#include "../draw.h"

void draw_ball(game_coord_t x, game_coord_t y) {
  //TODO get pd in here
  screen_coord_t left = x_game_to_screen(x);
  screen_coord_t top = y_game_to_screen(y);
  pd->graphics->fillEllipse(left, top,
			    BALL_RADIUS, BALL_RADIUS,
			    0.0f, 360.0f, kColorBlack);
}

void init_graphics(GameState* state) {
  //TODO get pd in here
  for (int i = 0; i < state->num_entities; ++i) {
    switch(state->obj_type[i]) {
    case BALL:
      draw_ball(pd, state->x_pos[i], state->y_pos[i]);
      break;
    case PADDLE:
      draw_paddle(pd, state->x_pos[i], state->y_pos[i]);
      break;
    case BLOCK:
      draw_block(pd, state->x_pos[i], state->y_pos[i]);
      break;
    default:
      pd->system->error("Unhandled entity type!");
    }
  }
}

