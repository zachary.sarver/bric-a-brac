#pragma once

#include "types.h"

/**
 * Gets input from the platform
 */
input_t get_input();

int eventHandler(void*);
