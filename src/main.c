#include <stdbool.h>
#include <stdlib.h>

#include "GameState.h"
#include "draw.h"
#include "game_logic.h"
#include "init.h"
#include "types.h"

GameState state; // global data, ooo, scary

static int update(void* userdata) {
  /* PlaydateAPI* pd = userdata; */
  GameChanges changes;
  input_t input = get_input(/* pd */);
  tick_game(input, &state, &changes);
  handle_collisions(&state, &changes);
  draw_changes(&changes);
  /* pd->system->drawFPS(0,0); */
  return 1;
}

int main (int argc, char **argv) {}
