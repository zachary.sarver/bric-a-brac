#pragma once

#include "types.h"

screen_coord_t x_game_to_screen(game_coord_t x);
screen_coord_t y_game_to_screen(game_coord_t y);
game_coord_t x_screen_to_game(screen_coord_t x);
game_coord_t y_screen_to_game(screen_coord_t y);
