#pragma once

#include <stdbool.h>

#include "types.h"

typedef struct GameState {
  game_coord_t ball_dx;
  game_coord_t ball_dy;
  game_id_t num_entities;
  game_coord_t x_pos[NUM_OBJ];
  game_coord_t y_pos[NUM_OBJ];
  obj_type_t obj_type[NUM_OBJ];
  bool alive[NUM_OBJ];
} GameState;

void insert_game_entity(GameState *state, game_id_t id, game_coord_t x_pos, game_coord_t y_pos);
void insert_block(GameState* state, game_id_t id, game_coord_t x_pos, game_coord_t y_pos);
