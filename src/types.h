#pragma once

#include <stdint.h>

typedef enum {
  NONE,
  BALL,
  PADDLE,
  BLOCK
} obj_type_t;

typedef int16_t game_coord_t;
typedef uint16_t screen_coord_t;
typedef uint8_t game_id_t;
typedef float input_t;

#define N_BLOCKS 40
#define MAX_X_COORD INT16_MAX
#define MAX_Y_COORD ((3 * MAX_X_COORD)/5)
#define MIN_COORD 0
#define NUM_OBJ 50
#define BALL_ID 0
#define PADDLE_ID 1
#define BLOCK_WIDTH (MAX_X_COORD / 11)
#define BLOCK_HEIGHT (MAX_Y_COORD / 7)
#define BALL_RADIUS 16
