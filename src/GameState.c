#include "GameState.h"

void insert_game_entity(GameState *state, game_id_t id, game_coord_t x_pos, game_coord_t y_pos) {
  state->x_pos[id] = x_pos;
  state->y_pos[id] = y_pos;
  state->num_entities += 1;
}

void insert_block(GameState* state, game_id_t id, game_coord_t x_pos, game_coord_t y_pos) {
  insert_game_entity(state, id, x_pos, y_pos);
  state->obj_type[id] = BLOCK;
  state->alive[id] = true;
}
