#include <stdlib.h>

#include "GameState.h"
#include "init.h"
#include "types.h"

void init_ball(GameState* state) {
  const game_coord_t starting_ball_x = MAX_X_COORD >> 1;
  const game_coord_t starting_ball_y = MAX_Y_COORD - (MAX_Y_COORD >> 1);
  const game_coord_t starting_dx = MAX_X_COORD >> 7;
  const game_coord_t starting_dy = MAX_Y_COORD >> 7;
  insert_game_entity(state, BALL_ID, starting_ball_x, starting_ball_y);
  state->obj_type[BALL_ID] = BALL;
  state->ball_dx = starting_dx;
  state->ball_dy = starting_dy;
}

void init_paddle(GameState* state) {
  const game_coord_t paddle_width = BLOCK_WIDTH << 1;
  const game_coord_t paddle_height = BLOCK_HEIGHT >> 1;
  const game_coord_t starting_paddle_x = MAX_X_COORD >> 2;
  const game_coord_t starting_paddle_y = MAX_Y_COORD >> 7;
  insert_game_entity(state, PADDLE_ID, starting_paddle_x, starting_paddle_y);
  state->obj_type[PADDLE_ID] = PADDLE;
}

void init_state(GameState* state) {
  // there are 40 blocks, 8x5
  // The empty space between and on either side of blocks should add up to three blocks worth
  // the blocks should only be in the top half
  init_ball(state);
  init_paddle(state);

  game_id_t id = PADDLE_ID + 1;
  const game_coord_t base_height = MAX_Y_COORD >> 1;
  const game_coord_t x_spacing = (9 * BLOCK_WIDTH) / 3;
  const game_coord_t y_spacing = (6 * BLOCK_WIDTH) >> 1;
  for (int i = 0; i < N_BLOCKS; ++i, ++id) {
    game_coord_t x = (i + 1) * x_spacing + i * BLOCK_WIDTH;
    game_coord_t y = (i + 1) * y_spacing + i * BLOCK_HEIGHT;
    insert_block(state, id, x, y);
  }
}
