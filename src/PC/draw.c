#include "../GameState.h"
#include "../draw.h"
#include "../draw_univ.h"

const uint32_t screen_w = 1024;
const uint32_t screen_h = 768;

void draw_ball(game_coord_t x, game_coord_t y) {
  screen_coord_t left = x_game_to_screen(x);
  screen_coord_t top = y_game_to_screen(y);
  /* pd->graphics->fillEllipse(left, top, */
  /* 			    BALL_RADIUS, BALL_RADIUS, */
  /* 			    0.0f, 360.0f, kColorBlack); */
}

void init_graphics(GameState* state) {
  for (int i = 0; i < state->num_entities; ++i) {
    switch(state->obj_type[i]) {
    case BALL:
      /* draw_ball(pd, state->x_pos[i], state->y_pos[i]); */
      break;
    case PADDLE:
      /* draw_paddle(pd, state->x_pos[i], state->y_pos[i]); */
      break;
    case BLOCK:
      /* draw_block(pd, state->x_pos[i], state->y_pos[i]); */
      break;
    default:
      /* pd->system->error("Unhandled entity type!"); */
    }
  }
}

void draw_changes(GameChanges* changes) {}
