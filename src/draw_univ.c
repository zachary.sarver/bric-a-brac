#include "draw_univ.h"

const extern uint32_t screen_w;
const extern uint32_t screen_h;

screen_coord_t x_game_to_screen(game_coord_t x) {
  uint32_t num = screen_w * x;
  uint32_t den = MAX_X_COORD;
  return (screen_coord_t)(num/den);
}

screen_coord_t y_game_to_screen(game_coord_t y) {
  uint32_t num = screen_h * y;
  uint32_t den = MAX_Y_COORD;
  return (screen_coord_t)(num/den);
}

game_coord_t x_screen_to_game(screen_coord_t x) {
  uint32_t num = MAX_X_COORD * x;
  return (game_coord_t)(num / screen_w);
}

game_coord_t y_screen_to_game(screen_coord_t y) {
  uint32_t num = MAX_Y_COORD * y;
  return (game_coord_t)(num / screen_h);
}
