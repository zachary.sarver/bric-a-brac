#pragma once

#include "types.h"

typedef struct GameState GameState;
typedef struct GameChanges GameChanges;

void tick_game(input_t input, GameState* state, GameChanges* changes);
void handle_collisions(GameState* state, GameChanges* changes);
